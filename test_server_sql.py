import json
import requests
import time 

data = {
    "gap": {
        "val1": {
            "val_current_relay": 1,
            "val_sensor_speed": 3,
            "overheating_motor": 33,
            "val_clamping_on": 44,
            "val_speed": 443,
            "val_quantity_revers": 33,
        },
        "val2": {
            "val_current_relay": 1,
            "val_sensor_speed": 3,
            "overheating_motor": 33,
            "val_clamping_on": 44,
            "val_speed": 443,
            "val_quantity_revers": 33,
        },
        "val3": {
            "val_current_relay": 1,
            "val_sensor_speed": 3,
            "overheating_motor": 33,
            "val_clamping_on": 44,
            "val_speed": 443,
            "val_quantity_revers": 33,
        },
        "val4": {
            "val_current_relay": 1,
            "val_sensor_speed": 3,
            "overheating_motor": 33,
            "val_clamping_on": 44,
            "val_speed": 443,
            "val_quantity_revers": 33,
        },
        "val5": {
            "val_current_relay": 1,
            "val_sensor_speed": 3,
            "overheating_motor": 33,
            "val_clamping_on": 44,
            "val_speed": 443,
            "val_quantity_revers": 33,
        },
        "val6": {
            "val_current_relay": 1,
            "val_sensor_speed": 3,
            "overheating_motor": 33,
            "val_clamping_on": 44,
            "val_speed": 443,
            "val_quantity_revers": 33,
        }
    },
    "hamer": {
        "hammer_on_off": 77,
        "hammer_crash_overload": 66,
        "hammer_speed": 55,
        "hammer_conveyor_off": 45,
    },
    "conveyor1": {
        "conveyor_on_off_1": 22,
        "conveyor_current_relay_1": 32,
    },
    "conveyor2": {
        "conveyor_on_off_2": 34,
        "conveyor_current_relay_2": 323,
    }
}

time_ = time.time()


out = json.dumps(data)


url = 'http://188.225.39.107/CRUSHER/'
x = requests.post(url, data={"action": "insert", "test": out, "time": time_})
print(x.text)
