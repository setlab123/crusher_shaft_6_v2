
import sys
import os
from PyQt5.QtWidgets import (QWidget, QLabel,QPushButton, QApplication,
    QLineEdit, QApplication, QMainWindow, QAction, qApp, QComboBox, QDialog)
from PyQt5.QtGui import QPixmap, QColor, QIcon, QFont
from PyQt5.QtCore import QCoreApplication, QTimer, QSize, QRect
import PyQt5.QtCore as QtCore
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtCore import pyqtSignal

class MyTableDialog(QDialog, ):
  __ui = None
  def __init__(self, parent):
    super(MyTableDialog, self).__init__(parent)
    ui = self.__ui = Ui_MyTableDialog()
    ui.setupUi(self)
    self.__my_setup_ui()

  def GetResult(self, *args, **kwds):
    self.__set_args_to_ui(*args, **kwds)
    if self.exec_() != self.Accepted:
      return
    return self.__get_res_from_ui()

  def __my_setup_ui(self):
    "Здесь дополнительно инициализируем интерфейс и связываем сигналы"
  def __set_args_to_ui(self, *args, **kwds):
    "Здесь устанавливаем данные в элементы интерфейса"
  def __get_res_from_ui(self):
    "Здесь вытаскиваем введённые в интерфейсе данные"

# Пример использования
class MyMainWindow(...):
  def GetUserTable(self, *args, **kwds):
    res = MyTableDialog(self).GetResult(*args, **kwds)
    if res is None:
      logging.info('Пользователь отказался от ввода')
    else:
      self.__parse_user_table_input(res)
